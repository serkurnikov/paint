//go:generate mockgen -package=$GOPACKAGE -source=$GOFILE -destination=mock.$GOFILE Appl,Repo

// Package app provides business logic.
package app

import (
	"context"
	"paint/internal/imageProcApi"
	api "paint/pkg/api/proto_files"
)

type (
	Ctx = context.Context

	Appl interface {
		imageProcApi.Api
	}

	Repo interface{}

	app struct {
		repo         Repo
		imageProcApi imageProcApi.Api
	}
)

func (a app) DrawCustomContours(ctx context.Context, in *api.ContoursRequest) (*api.DefaultReply, error) {
	return a.imageProcApi.DrawCustomContours(ctx, in)
}

func NewAppl(repo Repo, paintApi imageProcApi.Api) Appl {
	return &app{
		repo:         repo,
		imageProcApi: paintApi,
	}
}
