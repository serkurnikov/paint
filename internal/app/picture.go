package app

import (
	"context"
	api "paint/api/proto/pb"
	ipc "paint/pkg/imageProcessingService"
)

func (a App) CreateUnderPaintImage(ctx context.Context, in *api.UnderPaintRequest) (*api.DefaultReply, error) {
	_, _ = ipc.CreateUnderPaintImage(ipc.IParams{})
	return nil, nil
}

func (a App) DisplayMainContours(ctx context.Context, in *api.DisplayContoursRequest) (*api.DefaultReply, error) {
	req := api.DisplayContoursRequest{
		Picture:  in.Picture,
	}
	return a.imageProcApi.DisplayMainContours(ctx, &req)
}
