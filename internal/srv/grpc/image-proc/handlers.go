package image_proc

import (
	"context"
	"gocv.io/x/gocv"
	api "paint/api/proto/pb"
	ipc "paint/pkg/imageProcessingService"
)

func (s *service) CreateUnderPaintImage(ctx context.Context, in *api.UnderPaintRequest) (*api.DefaultReply, error) {
	_, _ = ipc.CreateUnderPaintImage(ipc.IParams{
		BaseP: ipc.BaseP{
			Mat:    gocv.Mat{},
			Pieces: ipc.DefaultPieces,
		},
		PyrShiftP: ipc.PyrShiftP{
			Sp:       in.Sp,
			Sr:       in.Sr,
			MaxLevel: in.MaxLevel,
		},
	})
	return &api.DefaultReply{OutPicture: nil}, nil
}

func (s *service) DisplayMainContours(ctx context.Context, in *api.DisplayContoursRequest) (*api.DefaultReply, error) {
	_, _ = ipc.DisplayMainContours(ipc.IParams{
		BaseP: ipc.BaseP{
			Mat:    gocv.Mat{},
			Pieces: ipc.DefaultPieces,
		},
	})
	return &api.DefaultReply{OutPicture: nil}, nil
}
