// Package openapi implements OpenAPI server.
package openapi

import (
	"context"
	"github.com/powerman/structlog"
	"google.golang.org/grpc"
	"paint/internal/app"
	api "paint/pkg/api/proto_files"
)

const (
	port = ":10000"
)

type service struct {
	appl app.Appl
	api.UnimplementedImageProcessingServiceServer
}

type (
	Ctx = context.Context
	Log = *structlog.Logger
)

func NewServer(appl app.Appl) *grpc.Server {

	/*srv := grpc.NewServer(
		grpc.KeepaliveParams(keepalive.ServerParameters{
			Time:    50 * time.Second,
			Timeout: 10 * time.Second,
		}),
		grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{
			MinTime:             30 * time.Second,
			PermitWithoutStream: true,
		}),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			unaryServerLogger,
			unaryServerRecover,
			unaryServerAccessLog,
		)),
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			streamServerLogger,
			streamServerRecover,
			streamServerAccessLog,
		)),
	)*/

	srv := grpc.NewServer()

	api.RegisterImageProcessingServiceServer(srv, &service{
		appl: appl,
	})

	return srv
}
