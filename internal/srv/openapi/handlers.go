package openapi

import (
	"context"
	api "paint/pkg/api/proto_files"
)

func (s service) DrawDefaultContours(ctx context.Context, request *api.ContoursRequest) (*api.DefaultReply, error) {
	//return s.appl.DrawCustomContours(ctx, request)
	return &api.DefaultReply{OutPicture: "test"}, nil
}
