package imageProcApi

import (
	"context"
	"net"
	"net/http"
	"paint/internal/cache"
	"paint/internal/cache/memory"
	api "paint/pkg/api/proto_files"
	"time"
)

const (
	cachedTime  = 5 * time.Minute
	clearedTime = 30 * time.Minute
)

type Api interface {
	DrawCustomContours(ctx context.Context, in *api.ContoursRequest) (*api.DefaultReply, error)
}

type imageProcApi struct {
	client  *http.Client
	storage cache.Storage
}

func New() Api {
	defaultTransport := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		MaxIdleConns:        20,
		MaxIdleConnsPerHost: 20,
		TLSHandshakeTimeout: 15 * time.Second,
	}

	client := &http.Client{
		Transport: defaultTransport,
		Timeout:   15 * time.Second,
	}

	return &imageProcApi{
		client:  client,
		storage: memory.InitCash(cachedTime, clearedTime),
	}
}
