package imageProcApi

import (
	"context"
	api "paint/api/proto/pb"
)

func (i imageProcApi) CreateUnderPaintImage(ctx context.Context, in *api.UnderPaintRequest) (*api.DefaultReply, error) {
	req := api.UnderPaintRequest{
		Picture:  in.Picture,
		Sp:       in.Sp,
		Sr:       in.Sr,
		MaxLevel: in.MaxLevel,
	}
	return i.processingServiceClient.CreateUnderPaintImage(ctx, &req)
}

func (i imageProcApi) DisplayMainContours(ctx context.Context, in *api.DisplayContoursRequest) (*api.DefaultReply, error) {
	req := api.DisplayContoursRequest{
		Picture: in.Picture,
	}
	return i.processingServiceClient.DisplayContours(ctx, &req)
}
