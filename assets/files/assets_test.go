package files

import (
	"paint/pkg/utils"
	"testing"
)

func TestAssets(t *testing.T) {
	err := Init()
	if err != nil {
		t.Errorf("failed run Init() func %v", err)
	}

	_, err = utils.ReadFile(utils.GetAssetsDir() + "\\files\\structure.json")
	if err != nil {
		t.Errorf("failed Read structure.json file %v", err)
	}
}
