package files

//go:generate gojson -name=Assets -input structure.json -o structure.go -pkg files

import (
	"encoding/json"
	"github.com/Jeffail/gabs/v2"
	"github.com/pkg/errors"
	"log"
	"paint/pkg/utils"
	"strings"
)

type File struct {
	Name string
	Path string
}

func Init() (err error) {
	jsonObj := gabs.New()
	information, err := utils.GetListingDirectoryInfo(utils.GetAssetsDir())
	if err != nil {
		_ = errors.Wrap(err, "error get files information: assets.Init()")
		return err
	}

	for _, info := range information {
		path := utils.ParsePath(info)

		if !info.IsDir {
			hierarchy := strings.Join(path[:len(path)-1], ".")

			err := jsonObj.ArrayConcatP(File{
				Name: info.Name,
				Path: info.Path,
			}, hierarchy)
			if err != nil {
				_ = errors.Wrap(err, "error add information to json: assets generate.go")
			}
		}
	}

	jsonMap := make(map[string]interface{})
	err = json.Unmarshal([]byte(jsonObj.String()), &jsonMap)
	if err != nil {
		return err
	}

	d, err := json.Marshal(jsonMap)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	_ = utils.WriteFile(d, "/go/src/gitlab.com/serkurnikov/paint/assets/files/structure/json")
	return err
}
