package htmlG

import (
	"encoding/json"
	"fmt"
	"github.com/rohanthewiz/element"
	"io/ioutil"
	"log"
	"os"
	"paint/pkg/utils"
)

const (
	Div    = "div"
	Id     = "id"
	Select = "select"
	Option = "option"
	Value  = "value"

	Br    = "br"

	Class    = "class"
	Style    = "style"
	Content  = "content"
	Nav      = "nav"
	Box      = "box"
	BoxColor = "boxColor"
	ToolBar  = "toolbar"
	Files    = "files"

	DefSizeBox = 250
)

type Page struct {
	Name  string
	Value string
}

func GenerateReport() {
	header, e := CreateHeader()

	p := ReadPages()
	navigation, e := CreateNavigationBar(e, p)
	body, e := CreateBody(e, p)
	script, _ := createScript(e)

	_ = utils.WriteFile([]byte(
		header+navigation+body+script),
		utils.GetHtmlDir()+"\\report.html")
	ClearPages()
}

func WritePage(p Page) {
	pages := ReadPages()
	pages = append(pages, p)

	b, err := json.Marshal(pages)

	f, err := utils.OpenFile(utils.GetHtmlDir() + "\\pages.json")
	if err != nil {
		log.Printf("Cannot open file%v", err)
	}
	defer f.Close()

	_, _ = f.Write(b)
}

func ReadPages() []Page {
	f, err := utils.OpenFile(utils.GetHtmlDir() + "\\pages.json")
	if err != nil {
		log.Printf("Cannot open file%v", err)
	}
	defer f.Close()

	byteValue, _ := ioutil.ReadAll(f)
	var pages []Page

	_ = json.Unmarshal(byteValue, &pages)
	return pages
}

func ClearPages() {
	_ = os.Remove(utils.GetHtmlDir() + "\\pages.json")
	f, _ := utils.CreateFile(utils.GetHtmlDir() + "\\pages.json")
	defer f.Close()
}

func CreateHeader() (string, func(el string, attrs ...string) element.Element) {
	e := element.New
	return e("style").R(`
		#content {
			margin-top: 50px;
		}
		
		#toolbar {
			background: grey;
			top: 0;
			left: 0;
			right: 0;
			height: 42px;
			border-bottom: 1px solid rgb(80, 80, 80);
		}
	
		#nav {
			float: left;
			margin-left: 10px;
			margin-top: 10px;
		}
	
		.box {
			position: relative;
			width: 100%;
			display: block;
			margin: 0 auto;
			padding: 0;
			text-align: left;
		}
	
		.boxColor {
			margin: 10px;
			padding: 20px 10px;
	
			color: #ffffff;
			font-size: 8pt;
			text-align: center;
			display: inline-block;
		}
	`), e
}

func CreateNavigationBar(e func(el string, attrs ...string) element.Element, pages []Page) (string, func(el string, attrs ...string) element.Element) {
	var str string
	for i, p := range pages {
		str += e(Option, Value, fmt.Sprintf("file%v", i)).R(p.Name)
	}

	return e(Div, Id, ToolBar).R(
		e(Div, Id, Nav).R(e(Select, Id, Files).R(
			str),
		),
	), e
}

func CreateBody(e func(el string, attrs ...string) element.Element, pages []Page) (string, func(el string, attrs ...string) element.Element) {
	var str string
	for i, p := range pages {
		str += e(Div, Id, fmt.Sprintf("file%v", i), Style, "display: none").R(p.Value)
	}

	return e(Div, Id, Content).R(str), e
}

func createScript(e func(el string, attrs ...string) element.Element) (string, func(el string, attrs ...string) element.Element) {
	return e("script").R(
		`(function () {
			var files = document.getElementById('files');
			var visible;
			files.addEventListener('change', onChange, false);

			function select(part) {
				if (visible)
					visible.style.display = 'none';
				visible = document.getElementById(part);
				if (!visible)
				return;
				files.value = part;
				visible.style.display = 'block';
				location.hash = part;
			}

		function onChange() {
			select(files.value);
				window.scrollTo(0, 0);
			}

			if (location.hash != "") {
				select(location.hash.substr(1));
				}
				if (!visible) {
					select("file0");
					}
		})();`), e
}

func BuildStyle(args []string) (result string) {
	for i := 0; i < len(args); i++ {
		result += fmt.Sprintf(args[i])
	}
	return
}

func BG(v interface{}) string {
	return fmt.Sprintf("background-color:%v;", v)
}

func Width(v interface{}) string {
	return fmt.Sprintf("width: %vpx;", v)
}

func WidthDef() string {
	return Width(DefSizeBox)
}
