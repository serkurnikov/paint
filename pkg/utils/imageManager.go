package utils

import "C"
import (
	"fmt"
	"github.com/lucasb-eyer/go-colorful"
	"gocv.io/x/gocv"
	"image"
)

type ImageResult interface {
	Index() int
	Result() *gocv.Mat
}

func ReadImage(buf []byte, flags gocv.IMReadFlag) (img gocv.Mat, err error) {
	img, err = gocv.IMDecode(buf, flags)
	if err != nil {
		fmt.Printf("Failed to read image: gocv.IMDecode failed")
		return gocv.Mat{}, err
	}
	return img, err
}

func WriteImage(fileExt gocv.FileExt, img gocv.Mat) (buf []byte, err error) {
	buf, err = gocv.IMEncode(fileExt, img)
	if err != nil {
		fmt.Printf("Failed to read image: gocv.IMEncode failed")
		return nil, err
	}
	return buf, err
}

func Crop(mat gocv.Mat, left, top, right, bottom int) gocv.Mat {
	croppedMat := mat.Region(image.Rect(left, top, right, bottom))
	resultMat := croppedMat.Clone()
	return resultMat
}

func SplitMatV(mat gocv.Mat, pieces int) []gocv.Mat {
	result := make([]gocv.Mat, 0)
	h := mat.Rows() / pieces
	w := mat.Cols()

	for i := 0; i < pieces; i++ {
		bottom := i*h + h
		if i == pieces-1 {
			bottom = mat.Rows()
		}
		result = append(result, Crop(mat, 0, i*h, w, bottom))
	}
	return result
}

func MergeMatsV(ims []ImageResult) (gocv.Mat, error) {
	switch len(ims) {
	case 1:
		return *ims[0].Result(), nil
	default:
		dest := gocv.NewMat()
		for i := 0; i < len(ims); i++ {
			if ims[i].Index() == 0 {
				gocv.Vconcat(*ims[i].Result(), *ims[i+1].Result(), &dest)
				i++
			} else {
				gocv.Vconcat(dest, *ims[i].Result(), &dest)
			}
		}
		return dest, nil
	}
}

func GetVecbAt(m *gocv.Mat, row int, col int) gocv.Vecb {
	return m.GetVecbAt(row, col)
}

func SetVecbAt(m *gocv.Mat, row int, col int, v gocv.Vecb) {
	ch := m.Channels()
	for c := 0; c < ch; c++ {
		m.SetUCharAt(row, col*ch+c, v[c])
	}
}

func Clr2Vec(c colorful.Color) gocv.Vecb {
	R, G, B := c.RGB255()
	return gocv.Vecb{
		B, G, R,
	}
}

func Vec2Clr(v gocv.Vecb) colorful.Color {
	return colorful.Color{
		B: (float64(v[0]) - 0.5)/255,
		G: (float64(v[1]) - 0.5)/255,
		R: (float64(v[2]) - 0.5)/255,
	}.Clamped()
}
