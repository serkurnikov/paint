package utils

import (
	"github.com/lucasb-eyer/go-colorful"
	"gocv.io/x/gocv"
	"testing"
)

const (
	TestColorHex = "#e5af52"
	TestRC       = 2
)

func TestSetVecbAt(t *testing.T) {
	m := gocv.NewMatWithSize(TestRC, TestRC, gocv.MatTypeCV8U)
	c, _ := colorful.Hex(TestColorHex)
	vecb := Clr2Vec(c)

	for r := 0; r < m.Rows(); r++ {
		for c := 0; c < m.Cols(); c++ {
			SetVecbAt(&m, r, c, vecb)
		}
	}

	resultVecb := m.GetVecbAt(0,0)
	if resultVecb[0] != vecb[0] {
		t.Errorf("uncorrect fill mat: Clr2Vec & SetVecbAt")
	}
}
