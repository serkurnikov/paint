package utils

import (
	"encoding/json"
	"strings"
	"testing"
)

type TestData struct {
	T0 string  `json:"T0"`
	T1 []int32 `json:"T1"`
	T2 bool    `json:"T2"`
}

func TestStructToMap(t *testing.T) {
	data := &TestData{
		T0: "test",
		T1: []int32{1, 2},
		T2: false,
	}
	_, err := StructToMap(data)
	if err != nil {
		t.Errorf("Cannot convert struct to map:%v", err)
	}
}

func TestSetField(t *testing.T) {
	data := TestData{
		T0: "test",
		T1: []int32{1, 2},
		T2: false,
	}

	update := "test_update"

	err := SetField(&data, "T0", update)
	if err != nil {
		t.Errorf("Cannot set value:%v", err)
	}

	if strings.Compare(data.T0, "test_update") != 0 {
		t.Errorf("Cannot set value: got %v, want %v", data.T0, update)
	}
}

func TestFillStruct(t *testing.T) {
	data := &TestData{}

	var params []interface{}
	params = append(params, "test")
	params = append(params, []int{1, 2})
	params = append(params, true)

	b, err := FillStruct(data, ToSlice(params))
	if err != nil {
		t.Errorf("Cannot fill struct:%v", err)
	}

	_ = json.Unmarshal(b, &data)

	if strings.Compare(data.T0, params[0].(string)) != 0 {
		t.Errorf("Uncorrect data got %v, want %v", data.T0, params[0].(string))
	}
}
