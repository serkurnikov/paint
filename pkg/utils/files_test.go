package utils

import (
	"os"
	"testing"
)

const (
	testFileCSV = "\\test.csv"
)

var testData = [][]string{
	{"C1", "C2", "C3"},
	{"C1_1", "C2_1", "C3_1"},
	{"C1_2", "C2_2", "C3_2"},
	{"C1_3", "C2_3", "C3_3"},
}

func TestWriteFileCSV(t *testing.T) {
	err := WriteFileCSV(GetAssetsDir()+testFileCSV, testData)
	if err != nil {
		t.Errorf("Cannot write file = %v", err)
	}
}

func TestReadFileCSV(t *testing.T) {
	csvLines, err := ReadFileCSV(GetAssetsDir() + testFileCSV)
	if err != nil {
		t.Errorf("failed read file = %v", err)
	}

	for i, line := range csvLines {
		for j := 0; j < len(line); j++ {
			if testData[i][j] != line[j] {
				t.Errorf("uncorrect data in file")
			}
		}
	}

	err = os.Remove(GetAssetsDir() + testFileCSV)
	if err != nil {
		t.Errorf("failed delete file = %v", err)
	}
}
