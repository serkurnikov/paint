package neural_network

import (
	"encoding/json"
	"fmt"
	"math"
	"math/rand"
	"os"
	ips "paint/pkg/imageProcessingService"
	"paint/pkg/models"
	"time"
)

type NeuralNetwork struct {
	HiddenLayer      []float64
	InputLayer       []float64
	OutputLayer      []float64
	WeightHidden     [][]float64
	WeightOutput     [][]float64
	ErrOutput        []float64
	ErrHidden        []float64
	LastChangeHidden [][]float64
	LastChangeOutput [][]float64
	Regression       bool
	Rate1            float64 //learning rate
	Rate2            float64
}

func sigmoid(X float64) float64 {
	return 1.0 / (1.0 + math.Pow(math.E, -X))
}

func dsigmoid(Y float64) float64 {
	return Y * (1.0 - Y)
}

func DumpNN(fileName string, nn *NeuralNetwork) {
	outF, err := os.OpenFile(fileName, os.O_CREATE|os.O_RDWR, 0777)
	if err != nil {
		panic("failed to dump the network to " + fileName)
	}
	defer outF.Close()
	encoder := json.NewEncoder(outF)
	err = encoder.Encode(nn)
	if err != nil {
		panic(err)
	}
}

func LoadNN(fileName string) *NeuralNetwork {
	inF, err := os.Open(fileName)
	if err != nil {
		panic("failed to load " + fileName)
	}
	defer inF.Close()
	decoder := json.NewDecoder(inF)
	nn := &NeuralNetwork{}
	err = decoder.Decode(nn)
	if err != nil {
		panic(err)
	}
	return nn
}

func makeMatrix(rows, columns int, value float64) [][]float64 {
	mat := make([][]float64, rows)
	for i := 0; i < rows; i++ {
		mat[i] = make([]float64, columns)
		for j := 0; j < columns; j++ {
			mat[i][j] = value
		}
	}
	return mat
}

func randomMatrix(rows, columns int, lower, upper float64) [][]float64 {
	mat := make([][]float64, rows)
	for i := 0; i < rows; i++ {
		mat[i] = make([]float64, columns)
		for j := 0; j < columns; j++ {
			mat[i][j] = rand.Float64()*(upper-lower) + lower
		}
	}
	return mat
}

func DefaultNetwork(iInputCount, iHiddenCount, iOutputCount int, iRegression bool) *NeuralNetwork {
	return NewNetwork(iInputCount, iHiddenCount, iOutputCount, iRegression, 0.25, 0.1)
}

func NewNetwork(iInputCount, iHiddenCount, iOutputCount int, iRegression bool, iRate1, iRate2 float64) *NeuralNetwork {
	iInputCount += 1
	iHiddenCount += 1
	rand.Seed(time.Now().UnixNano())
	network := &NeuralNetwork{}
	network.Regression = iRegression
	network.Rate1 = iRate1
	network.Rate2 = iRate2
	network.InputLayer = make([]float64, iInputCount)
	network.HiddenLayer = make([]float64, iHiddenCount)
	network.OutputLayer = make([]float64, iOutputCount)
	network.ErrOutput = make([]float64, iOutputCount)
	network.ErrHidden = make([]float64, iHiddenCount)

	network.WeightHidden = randomMatrix(iHiddenCount, iInputCount, -1.0, 1.0)
	network.WeightOutput = randomMatrix(iOutputCount, iHiddenCount, -1.0, 1.0)

	network.LastChangeHidden = makeMatrix(iHiddenCount, iInputCount, 0.0)
	network.LastChangeOutput = makeMatrix(iOutputCount, iHiddenCount, 0.0)

	return network
}

func (n *NeuralNetwork) Forward(input []float64) (output []float64) {
	if len(input)+1 != len(n.InputLayer) {
		panic("amount of input variable doesn't match")
	}
	for i := 0; i < len(input); i++ {
		n.InputLayer[i] = input[i]
	}
	n.InputLayer[len(n.InputLayer)-1] = 1.0 //bias node for input layer

	for i := 0; i < len(n.HiddenLayer)-1; i++ {
		sum := 0.0
		for j := 0; j < len(n.InputLayer); j++ {
			sum += n.InputLayer[j] * n.WeightHidden[i][j]
		}
		n.HiddenLayer[i] = sigmoid(sum)
	}

	n.HiddenLayer[len(n.HiddenLayer)-1] = 1.0 //bias node for hidden layer
	for i := 0; i < len(n.OutputLayer); i++ {
		sum := 0.0
		for j := 0; j < len(n.HiddenLayer); j++ {
			sum += n.HiddenLayer[j] * n.WeightOutput[i][j]
		}
		if n.Regression {
			n.OutputLayer[i] = sum
		} else {
			n.OutputLayer[i] = sigmoid(sum)
		}
	}
	return n.OutputLayer[:]
}

func (n *NeuralNetwork) Feedback(target []float64) {
	for i := 0; i < len(n.OutputLayer); i++ {
		n.ErrOutput[i] = n.OutputLayer[i] - target[i]
	}

	for i := 0; i < len(n.HiddenLayer)-1; i++ {
		err := 0.0
		for j := 0; j < len(n.OutputLayer); j++ {
			if n.Regression {
				err += n.ErrOutput[j] * n.WeightOutput[j][i]
			} else {
				err += n.ErrOutput[j] * n.WeightOutput[j][i] * dsigmoid(n.OutputLayer[j])
			}
		}
		n.ErrHidden[i] = err
	}

	for i := 0; i < len(n.OutputLayer); i++ {
		for j := 0; j < len(n.HiddenLayer); j++ {
			change := 0.0
			delta := 0.0
			if n.Regression {
				delta = n.ErrOutput[i]
			} else {
				delta = n.ErrOutput[i] * dsigmoid(n.OutputLayer[i])
			}
			change = n.Rate1*delta*n.HiddenLayer[j] + n.Rate2*n.LastChangeOutput[i][j]
			n.WeightOutput[i][j] -= change
			n.LastChangeOutput[i][j] = change
		}
	}

	for i := 0; i < len(n.HiddenLayer)-1; i++ {
		for j := 0; j < len(n.InputLayer); j++ {
			delta := n.ErrHidden[i] * dsigmoid(n.HiddenLayer[i])
			change := n.Rate1*delta*n.InputLayer[j] + n.Rate2*n.LastChangeHidden[i][j]
			n.WeightHidden[i][j] -= change
			n.LastChangeHidden[i][j] = change
		}
	}
}

func (n *NeuralNetwork) CalcError(inputs []float64, targets []float64) float64 {
	//in, trg := ExtractData(inputs, targets)
	//errPortion := math.Sqrt(utils.Sq(trg.ShadeC2-o.ShadeC2) + utils.Sq(trg.ShadeC3-o.ShadeC3))
	in, o := models.ExtractData(inputs, n.OutputLayer)
	errDiff := ips.CheckBlendColorDiff(in, o)

	return 0.5 * errDiff * errDiff
}

func (n *NeuralNetwork) CalcErrorDefault(target []float64) float64 {
	errSum := 0.0
	for i := 0; i < len(n.OutputLayer); i++ {
		err := n.OutputLayer[i] - target[i]
		errSum += 0.5 * err * err
	}
	return errSum
}

func genRandomIdx(N int) []int {
	A := make([]int, N)
	for i := 0; i < N; i++ {
		A[i] = i
	}
	//randomize
	for i := 0; i < N; i++ {
		j := i + int(rand.Float64()*float64(N-i))
		A[i], A[j] = A[j], A[i]
	}
	return A
}

func (n *NeuralNetwork) Train(inputs [][]float64, targets [][]float64, iteration int) {
	if len(inputs[0])+1 != len(n.InputLayer) {
		panic("amount of input variable doesn't match")
	}
	if len(targets[0]) != len(n.OutputLayer) {
		panic("amount of output variable doesn't match")
	}

	iterFlag := -1
	for i := 0; i < iteration; i++ {
		idxAry := genRandomIdx(len(inputs))
		curErr := 0.0
		for j := 0; j < len(inputs); j++ {
			n.Forward(inputs[idxAry[j]])
			n.Feedback(targets[idxAry[j]])
			curErr += n.CalcError(inputs[idxAry[j]], targets[idxAry[j]])
			if (j+1)%1000 == 0 {
				if iterFlag != i {
					fmt.Println("")
					iterFlag = i
				}
				fmt.Printf("iteration %vth / progress %.2f %% \r", i+1, float64(j)*100/float64(len(inputs)))
			}
		}
		if (iteration >= 10 && (i+1)%(iteration/10) == 0) || iteration < 10 {
			fmt.Printf("\niteration %vth MSE: %.5f", i+1, curErr/float64(len(inputs)))
		}
	}
	fmt.Println("\ndone.")
}

func (n *NeuralNetwork) TrainMap(inputs []map[int]float64, targets [][]float64, iteration int) {
	if len(targets[0]) != len(n.OutputLayer) {
		panic("amount of output variable doesn't match")
	}

	iterFlag := -1
	for i := 0; i < iteration; i++ {
		idxAry := genRandomIdx(len(inputs))
		curErr := 0.0
		for j := 0; j < len(inputs); j++ {
			n.ForwardMap(inputs[idxAry[j]])
			n.FeedbackMap(targets[idxAry[j]], inputs[idxAry[j]])
			curErr += n.CalcErrorDefault(targets[idxAry[j]])
			if (j+1)%1000 == 0 {
				if iterFlag != i {
					fmt.Println("")
					iterFlag = i
				}
				fmt.Printf("iteration %vth / progress %.2f %% \r", i+1, float64(j)*100/float64(len(inputs)))
			}
		}
		if (iteration >= 10 && (i+1)%(iteration/10) == 0) || iteration < 10 {
			fmt.Printf("\niteration %vth MSE: %.5f", i+1, curErr/float64(len(inputs)))
		}
	}
	fmt.Println("\ndone.")
}

func (n *NeuralNetwork) ForwardMap(input map[int]float64) (output []float64) {
	for k, v := range input {
		n.InputLayer[k] = v
	}
	n.InputLayer[len(n.InputLayer)-1] = 1.0 //bias node for input layer

	for i := 0; i < len(n.HiddenLayer)-1; i++ {
		sum := 0.0
		for j, _ := range input {
			sum += n.InputLayer[j] * n.WeightHidden[i][j]
		}
		n.HiddenLayer[i] = sigmoid(sum)
	}

	n.HiddenLayer[len(n.HiddenLayer)-1] = 1.0 //bias node for hidden layer
	for i := 0; i < len(n.OutputLayer); i++ {
		sum := 0.0
		for j := 0; j < len(n.HiddenLayer); j++ {
			sum += n.HiddenLayer[j] * n.WeightOutput[i][j]
		}
		if n.Regression {
			n.OutputLayer[i] = sum
		} else {
			n.OutputLayer[i] = sigmoid(sum)
		}
	}
	return n.OutputLayer[:]
}

func (n *NeuralNetwork) FeedbackMap(target []float64, input map[int]float64) {
	for i := 0; i < len(n.OutputLayer); i++ {
		n.ErrOutput[i] = n.OutputLayer[i] - target[i]
	}

	for i := 0; i < len(n.HiddenLayer)-1; i++ {
		err := 0.0
		for j := 0; j < len(n.OutputLayer); j++ {
			if n.Regression {
				err += n.ErrOutput[j] * n.WeightOutput[j][i]
			} else {
				err += n.ErrOutput[j] * n.WeightOutput[j][i] * dsigmoid(n.OutputLayer[j])
			}

		}
		n.ErrHidden[i] = err
	}

	for i := 0; i < len(n.OutputLayer); i++ {
		for j := 0; j < len(n.HiddenLayer); j++ {
			change := 0.0
			delta := 0.0
			if n.Regression {
				delta = n.ErrOutput[i]
			} else {
				delta = n.ErrOutput[i] * dsigmoid(n.OutputLayer[i])
			}
			change = n.Rate1*delta*n.HiddenLayer[j] + n.Rate2*n.LastChangeOutput[i][j]
			n.WeightOutput[i][j] -= change
			n.LastChangeOutput[i][j] = change

		}
	}

	for i := 0; i < len(n.HiddenLayer)-1; i++ {
		for j, _ := range input {
			delta := n.ErrHidden[i] * dsigmoid(n.HiddenLayer[i])
			change := n.Rate1*delta*n.InputLayer[j] + n.Rate2*n.LastChangeHidden[i][j]
			n.WeightHidden[i][j] -= change
			n.LastChangeHidden[i][j] = change

		}
	}
}
