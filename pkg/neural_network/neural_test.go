package neural_network

import (
	"github.com/lucasb-eyer/go-colorful"
	"github.com/rohanthewiz/element"
	h "paint/pkg/htmlG"
	ips "paint/pkg/imageProcessingService"
	"paint/pkg/models"
	"paint/pkg/neural_network/configs"
	"testing"
)

const (
	c0 = iota
	c1
	c2
	c3
)

func getConfig() {
	return
}
//TODO auto generation test data
func TestCreateTrainDataForNN(t *testing.T) {
	neural := &Neural{}
	neural.cfg, _ = configs.New()

	err := neural.CreateTrainDataForNN()
	if err != nil {
		t.Errorf("Cannot create train Data %v", err)
	}

	inputData, targetData, _ := models.LoadData(neural.cfg.ClrNN.InputCount, neural.cfg.ClrNN.TrainData)
	GenerateHtmlResult(inputData, targetData)
}

func TestInitColorNN(t *testing.T) {
	neural := &Neural{}
	neural.cfg, _ = configs.New()

	_, _ = neural.InitColorNN()
}

func TestColorNeuralAccuracy(t *testing.T) {
	cfg, _ := configs.New()
	neural := LoadNN(cfg.ClrNN.Name)

	inputData, _, err := models.LoadData(cfg.ClrNN.InputCount, cfg.ClrNN.TrainData)
	if err != nil {
		t.Errorf("Cannot load test data %v", err)
	}

	var truePosNeg, sumDiff float64
	numPredictions := len(inputData)

	var neuralResult [][]float64
	for _, input := range inputData {

		resNeural := neural.Forward(input)
		neuralResult = append(neuralResult, resNeural)
		in, o := models.ExtractData(input, resNeural)

		diff := ips.CheckBlendColorDiff(in, o)
		sumDiff += diff
		if diff < ips.DefaultDiff {
			truePosNeg++
		}
	}
	GenerateHtmlResult(inputData, neuralResult)
	t.Logf("Accurancy %v, average color diff:%0.3f",
		truePosNeg/float64(numPredictions)*100,
		sumDiff/float64(numPredictions))
}

func TestRunNeuralNetwork(t *testing.T) {
	neural := &Neural{}
	err := neural.runNeuralNetwork()
	if err != nil {
		t.Errorf("Cannot run neural network")
	}
}

func GenerateHtmlResult(inputData, targetData [][]float64) {
	e := element.New
	var s string

	for i := 0; i < len(inputData); i++ {

		in, o := models.ExtractData(inputData[i], targetData[i])

		for j := 0; j <= subsetSize; j++ {
			p := make([]string, 0)
			switch j {
			//Display colors
			case c0:
				p = append(p, h.BG(
					colorful.Luv(
						in.LR,
						in.UR,
						in.VR).Hex()))
				p = append(p, h.Width(h.DefSizeBox))
			case c1:
				p = append(p, h.BG(
					colorful.Luv(
						o.LC1,
						o.UC1,
						o.VC1).Hex()))
				p = append(p, h.Width(h.DefSizeBox))
			case c2:
				p = append(p, h.BG(
					colorful.Luv(
						o.LC2,
						o.UC2,
						o.VC2).Hex()))
				p = append(p, h.Width(h.DefSizeBox*o.ShadeC2))
			case c3:
				p = append(p, h.BG(
					colorful.Luv(
						o.LC3,
						o.UC3,
						o.VC3).Hex()))
				p = append(p, h.Width(h.DefSizeBox*o.ShadeC3))
			}
			s += e(h.Div, h.Class, h.BoxColor, h.Style, h.BuildStyle(p)).R()
		}
		s += e(h.Br).R()
	}

	h.WritePage(h.Page{
		Name:  "Neural calculation separation",
		Value: s,
	})
}
