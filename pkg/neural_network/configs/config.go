package configs

import (
	"flag"
	"fmt"
	"gopkg.in/yaml.v3"
	"log"
	"os"
	"sync"
)

var (
	once sync.Once
	instance *ConfigNeural
)

type ConfigNeural struct {
	ClrNN struct {
		Name        string `yaml:"name"`
		Accuracy    float64 `yaml:"accuracy"`
		InputCount  int    `yaml:"inputCount"`
		HiddenCount int    `yaml:"hiddenCount"`
		OutputCount int    `yaml:"outputCount"`
		Regression  bool   `yaml:"regression"`
		Iterations  int    `yaml:"iterations"`
		TrainData   string `yaml:"trainData"`
		TestData    string `yaml:"testData"`
		InputData   string `yaml:"inputData"`
		TargetData  string `yaml:"targetData"`
	} `yaml:"colorNeural"`
}

func New() (*ConfigNeural, error) {
	once.Do(func() {
		instance, _ = Init()
	})

	return instance, nil
}

func Init() (*ConfigNeural, error) {
	cfgPath, err := parseFlags()
	if err != nil {
		log.Fatal(err)
	}
	cfg, err := newConfig(cfgPath)
	if err != nil {
		log.Fatal(err)
	}
	return cfg, err
}

func newConfig(configPath string) (*ConfigNeural, error) {
	config := &ConfigNeural{}

	file, err := os.Open(configPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	d := yaml.NewDecoder(file)

	if err := d.Decode(&config); err != nil {
		return nil, err
	}

	return config, nil
}

func validateConfigPath(path string) error {
	s, err := os.Stat(path)
	if err != nil {
		return err
	}
	if s.IsDir() {
		return fmt.Errorf("'%s' is a directory, not a normal file", path)
	}
	return nil
}

func parseFlags() (string, error) {
	var configPath string

	flag.StringVar(&configPath, "config", "/go/src/gitlab.com/serkurnikov/paint/pkg/neural_network/configs/config.yml", "path to config file")

	flag.Parse()

	if err := validateConfigPath(configPath); err != nil {
		return "", err
	}

	return configPath, nil
}
