package imageProcessingService

import (
	"encoding/json"
	"github.com/Jeffail/gabs/v2"
	"github.com/lucasb-eyer/go-colorful"
	"paint/pkg/combs"
	"paint/pkg/models"
	"strconv"
	"strings"
)

const DefaultNumberOfShades = 10

type Color3 struct {
	C1, C2, C3 string
}

func BlendColors(colorS1, colorS2 string, numberOfShades int) *gabs.Container {
	jsonObj := gabs.New()

	c1, _ := colorful.Hex(colorS1)
	c2, _ := colorful.Hex(colorS2)

	for i := 0; i < numberOfShades; i++ {
		portion := float64(i) / float64(numberOfShades-1)
		_, _ = jsonObj.Set(c1.BlendLuv(c2, portion).Hex(), strconv.FormatFloat(portion, 'f', 6, 64))
	}
	return jsonObj
}

func Blend3Colors(color Color3, numberOfShades int) *gabs.Container {
	jsonObj := gabs.New()
	var colors map[string]interface{}
	_ = json.Unmarshal([]byte(BlendColors(color.C1, color.C2, numberOfShades).String()), &colors)

	for k, v := range colors {
		blending := BlendColors(v.(string), color.C3, numberOfShades)
		_, _ = jsonObj.Set(blending, color.C1, k+color.C2, v.(string))
	}
	return jsonObj
}

func BlendClrWithArrayOfColors(colorMainS string, arrayOfColors []string, numberOfShades int) *gabs.Container {
	jsonObj := gabs.New()
	for i := 0; i < len(arrayOfColors); i++ {
		if !jsonObj.Exists(arrayOfColors[i]) {
			result := BlendColors(colorMainS, arrayOfColors[i], numberOfShades)
			_, _ = jsonObj.Set(result, arrayOfColors[i])
		}
	}
	return jsonObj
}

func BlendCombination(combinationElements []string, numberOfShades int) *gabs.Container {
	jsonObj := gabs.New()
	for i := 0; i < len(combinationElements)-1; i++ {
		result := BlendClrWithArrayOfColors(combinationElements[i], combinationElements[i+1:], numberOfShades)
		_, _ = jsonObj.Set(result, combinationElements[i])
	}
	return jsonObj
}

func GetAllMixColors(colorsDataS []string, numberOfShades int) *gabs.Container {
	jsonObj := gabs.New()
	subsets := combs.All(colorsDataS)
	for i := 0; i < len(subsets); i++ {
		result := BlendCombination(subsets[i], numberOfShades)
		_, _ = jsonObj.Set(result, strings.Join(subsets[i], ""))
	}
	return jsonObj
}

func CheckBlendColorDiff(i models.InputData, o models.OutData) float64 {
	c1 := colorful.Luv(o.LC1, o.UC1, o.VC1)
	c2 := colorful.Luv(o.LC2, o.UC2, o.VC2)
	c3 := colorful.Luv(o.LC3, o.UC3, o.VC3)

	result := c1.BlendLuv(c2, o.ShadeC2).BlendLuv(c3, o.ShadeC3)

	return result.DistanceLuv(colorful.Luv(i.LR, i.UR, i.VR))
}

func CheckClrDiffByHex(iHex, oHex string) float64 {
	c1, _ := colorful.Hex(iHex)
	c2, _ := colorful.Hex(oHex)

	return c1.DistanceLuv(c2)
}
