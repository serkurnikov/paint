package imageProcessingService

import (
	"fmt"
	"github.com/lucasb-eyer/go-colorful"
	"github.com/pkg/errors"
	"gocv.io/x/gocv"
	"image"
	"log"
	"math"
	"paint/pkg/def"
	u "paint/pkg/utils"
	"sort"
	"sync"
)

func PyrMeanShiftFilteringC(p IParams) (result []byte, err error) {
	result, err = Processing(p, shiftFiltering)
	if err != nil {
		_ = errors.Wrap(err, "failed starting pyrMeanShiftFiltering operation: Core")
		return nil, err
	}
	return result, err
}

//MaxLevel int32
//Sp       float32
//Sr       float32
func shiftFiltering(p IParams) (interface{}, error) {
	defer p.Mat.Close()

	imageSegment := gocv.NewMat()

	gocv.PyrMeanShiftFiltering(p.Mat, &imageSegment, float64(p.Sp), float64(p.Sr), int(p.MaxLevel))
	return &imageSegment, nil
}

func displayColorArea(m *gocv.Mat, a ColorArea) *gocv.Mat {
	clr, _ := colorful.Hex(a.BlendStr.ResultHex)
	for _, p := range a.Points {
		u.SetVecbAt(m, p.R, p.C, u.Clr2Vec(clr))
	}
	return m
}

//TODO add concurrent (divide map into pieces):
//	- add opportunity to save data results to cash
func displayInDominatedClrs(m *gocv.Mat, clusters int) *gocv.Mat {
	clrs := buildPallet(clusters)
	var wg sync.WaitGroup
	var mu sync.Mutex

	for r := 0; r < m.Rows(); r++ {
		for c := 0; c < m.Cols(); c++ {
			wg.Add(1)
			go func(r, c int, wg *sync.WaitGroup, mu *sync.Mutex) {
				clr := getSimilarClr(&clrs, u.Vec2Clr(m.GetVecbAt(r, c)))
				mu.Lock()
				u.SetVecbAt(m, r, c, u.Clr2Vec(clr))
				mu.Unlock()
				wg.Done()
			}(r, c, &wg, &mu)
		}
	}
	wg.Wait()
	return m
}

//TODO divide mat into pieces and use channel
// - merge chan IPoint into single channel
// - and concurrent StructAmidFabricColors
func calculateClrAreas(m *gocv.Mat, clusters int) (colorsArea []*ColorArea, err error) {
	clrs := buildPallet(clusters)
	clrMap := make(map[colorful.Color][]*IPoint, 0)

	var wg sync.WaitGroup
	var mu sync.Mutex

	for r := 0; r < m.Rows(); r++ {
		for c := 0; c < m.Cols(); c++ {
			wg.Add(1)
			go func(r, c int, wg *sync.WaitGroup, mu *sync.Mutex) {
				clr := getSimilarClr(&clrs, u.Vec2Clr(m.GetVecbAt(r, c)))
				mu.Lock()
				clrMap[clr] = append(clrMap[clr], &IPoint{
					R: r,
					C: c,
				})
				mu.Unlock()
				wg.Done()
			}(r, c, &wg, &mu)
		}
	}
	wg.Wait()

	for clr, p := range clrMap {
		wg.Add(1)
		go func(clr colorful.Color, p []*IPoint, wg *sync.WaitGroup, mu *sync.Mutex) {
			mu.Lock()
			colorsArea = append(colorsArea, &ColorArea{
				BlendStr: StructAmidFabricColors(clr.Hex(), MasterColors, TypeLuv)[0],
				Points:   p,
			})
			mu.Unlock()
			wg.Done()
		}(clr, p, &wg, &mu)
	}
	wg.Wait()

	return colorsArea, nil
}

//TODO rewrite method to mat operations
func buildPallet(clusters int) (result []colorful.Color) {
	img, _ := u.LoadImage(u.BuildImagePath("shiftFiltering"))
	items, err := KmeansWithAll(clusters, img, ArgumentDefault, DefaultSize, GetDefaultMasks())
	if err != nil {
		log.Println(err)
		return nil
	}

	for _, item := range items {
		clusterCol, _ := colorful.Hex(def.Lattice + item.AsString())
		result = append(result, clusterCol)
	}
	return result
}

//TODO optimize - add break if diff < Default diff (delete sort)
func getSimilarClr(clrs *[]colorful.Color, o colorful.Color) colorful.Color {
	similarClrs := make([]SimilarClr, 0)

	for _, c := range *clrs {
		similarClrs = append(similarClrs, SimilarClr{
			Hex:  c.Hex(),
			Diff: o.DistanceLuv(c),
		})
	}

	sort.Slice(similarClrs, func(i, j int) bool { return similarClrs[i].Diff < similarClrs[j].Diff })
	resClr, _ := colorful.Hex(similarClrs[0].Hex)
	return resClr
}

func averageClr(img image.Image, points []image.Point) (colorful.Color, error) {

	pixels := float64(len(points))
	var r, g, b float64 = 0, 0, 0

	for _, p := range points {
		cr, cg, cb, _ := img.At(p.X, p.Y).RGBA()
		r += math.Pow(float64(cr), 2)
		g += math.Pow(float64(cg), 2)
		b += math.Pow(float64(cb), 2)
	}

	r = math.Sqrt(r / pixels)
	g = math.Sqrt(g / pixels)
	b = math.Sqrt(b / pixels)

	hexClr := fmt.Sprintf("#%02X%02X%02X", int(r/0x0101), int(g/0x0101), int(b/0x0101))

	return colorful.Hex(hexClr)
}
