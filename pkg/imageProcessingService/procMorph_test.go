package imageProcessingService

import (
	"fmt"
	"gocv.io/x/gocv"
	"image/color"
	"math"
	"paint/pkg/utils"
	"testing"
)

const (
	TestImage  = "test0"
	TestPieces = 10
	//PyrMeanShiftFilter
	TestMaxLevel = 1
	TestSp       = 30
	TestSr       = 50
	//Morph func
	TestKernelSize = 3
	TestIterations = 1
	//Sobel & Laplacian
	TestDelta = 0
	TestScale = 1
	TestSize  = 1
	//Contours
	CannyT1               = 150
	CannyT2               = 200
	TestThick             = 1
	TestRetrievalMode     = gocv.RetrievalExternal
	TestApproxContourMode = gocv.ChainApproxSimple
	TSigmaX               = 1
	TSigmaY               = 1
	TBlurSize             = 1
)

var TestContourColor = color.RGBA{
	R: 255,
	G: 0,
	B: 0,
	A: 0}

var TestP = IParams{
	BaseP: BaseP{
		Pieces: TestPieces,
	},
	FBGroundP: FBGroundP{
		Shape:      gocv.MorphEllipse,
		KernelSize: TestKernelSize,
		Iterations: TestIterations,
	},
	MorphP: MorphP{
		Shape:      gocv.MorphEllipse,
		Border:     gocv.BorderDefault,
		KernelSize: TestKernelSize,
		Iterations: TestIterations,
	},
	PyrShiftP: PyrShiftP{
		MaxLevel: TestMaxLevel,
		Sp:       TestSp,
		Sr:       TestSr,
	},
	LaplacianP: LaplacianP{
		Border: gocv.BorderDefault,
		Delta:  TestDelta,
		Scale:  TestScale,
		Size:   TestSize,
	},
	SobelP: SobelP{
		Border:     gocv.BorderDefault,
		Delta:      TestDelta,
		Scale:      TestScale,
		KernelSize: TestKernelSize,
	},
	ThresholdP: ThresholdP{
		Thresh:   128,
		Maxvalue: 255,
	},
	ContourP: ContourP{
		Color:                    TestContourColor,
		T1:                       CannyT1,
		T2:                       CannyT2,
		Thickness:                TestThick,
		RetrievalMode:            gocv.RetrievalExternal,
		ContourApproximationMode: gocv.ChainApproxSimple,
	},
	ContourCustom: ContourCustom{
		Color:                    TestContourColor,
		T1:                       CannyT1,
		T2:                       CannyT2,
		BlurSize:                 TBlurSize,
		KernelSize:               TestKernelSize,
		Thickness:                TestThick,
		SigmaX:                   TSigmaX,
		SigmaY:                   TSigmaY,
		Border:                   gocv.BorderDefault,
		RetrievalMode:            TestRetrievalMode,
		ContourApproximationMode: TestApproxContourMode,
		Type:                     gocv.MorphClose,
		Shape:                    gocv.MorphEllipse,
	},

	HoughCircle: HoughCircle{
		Color:     TestContourColor,
		Thickness: TestThick,
		HMode:     gocv.HoughGradient,
		Dp:        5,
		MinDist:   100,
		p1:        100,
		p2:        100,
		MinRadius: 0,
		MaxRadius: 0,
	},
	HoughLine: HoughLine{
		Color:         TestContourColor,
		Thickness:     TestThick,
		Rho:           2,
		Theta:         math.Pi/360,
		minLineLength: 100,
		maxLineGap:    1,
		Threshold:     50,
	},
}

func TestShiftFiltering(t *testing.T) {
	TestP.Mat = gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadColor)
	mat, err := shiftFiltering(TestP)
	result := *mat.(*gocv.Mat)
	defer result.Close()

	if err != nil {
		t.Errorf("Error shiftFiltering %v", err)
	}
	gocv.IMWrite(utils.BuildImagePath("shiftFiltering"), result)
}

func TestSobel(t *testing.T) {
	TestP.Mat = gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadColor)
	mat, err := sobel(TestP)
	result := *mat.(*gocv.Mat)
	defer result.Close()

	if err != nil {
		t.Errorf("Error sobel %v", err)
	}
	gocv.IMWrite(utils.BuildImagePath("sobel"), result)
}

func TestLaplacian(t *testing.T) {
	TestP.Mat = gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadColor)
	mat, err := laplacian(TestP)
	result := *mat.(*gocv.Mat)
	defer result.Close()

	if err != nil {
		t.Errorf("Error laplacian %v", err)
	}
	gocv.IMWrite(utils.BuildImagePath("laplacian"), result)
}

func TestThreshold(t *testing.T) {
	TestP.Mat = gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadColor)
	mat, err := threshold(TestP)
	result := *mat.(*gocv.Mat)
	defer result.Close()

	if err != nil {
		t.Errorf("Error threshold %v", err)
	}
	gocv.IMWrite(utils.BuildImagePath("threshold"), result)
}

func TestMorphology(t *testing.T) {
	TestP.Mat = gocv.IMRead(utils.BuildImagePath("sobel"), gocv.IMReadColor)
	types := []gocv.MorphType{
		gocv.MorphErode,
		gocv.MorphDilate,
		gocv.MorphOpen,
		gocv.MorphClose,
		gocv.MorphGradient,
		gocv.MorphTophat,
		gocv.MorphBlackhat,
	}

	vars := make(map[interface{}]Calculation)

	for i := 0; i < len(types); i++ {
		TestP.MorphP.Type = types[i]
		imr := ImageResult{index: i}
		calculation := Calculation{prm: TestP, fn: morph}

		vars[&imr] = calculation
	}

	err := SetupProc(vars)
	if err != nil {
		t.Errorf("Error Morphology %v", err)
	}

	for v, c := range vars {
		name := fmt.Sprintf("%v", c.prm.MorphP.Type)
		gocv.IMWrite(utils.BuildImagePath(name), *v.(*ImageResult).result)
	}
}

func TestFbGround(t *testing.T) {
	TestP.Mat = gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadColor)
	mat, err := fBGround(TestP)
	result := *mat.(*gocv.Mat)
	defer result.Close()

	if err != nil {
		t.Errorf("Error FBGround %v", err)
	}
	gocv.IMWrite(utils.BuildImagePath("ground"), result)
}

func TestPencil(t *testing.T) {
	TestP.Mat = gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadGrayScale)
	mat, err := pencil(TestP)
	result := *mat.(*gocv.Mat)
	defer result.Close()

	if err != nil {
		t.Errorf("Error pencil %v", err)
	}
	gocv.IMWrite(utils.BuildImagePath("pencil"), result)
}

func BenchmarkShiftFiltering(b *testing.B) {
	TestP.Mat = gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadColor)
	for i := 0; i < b.N; i++ {
		_, _ = shiftFiltering(TestP)
	}
}

func BenchmarkShiftFilteringC(b *testing.B) {
	TestP.Mat = gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadColor)
	for i := 0; i < b.N; i++ {
		_, _ = PyrMeanShiftFilteringC(TestP)
	}
}
