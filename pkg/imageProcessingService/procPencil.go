package imageProcessingService

import (
	"github.com/pkg/errors"
	"gocv.io/x/gocv"
)

func PencilC(p IParams) (result []byte, err error) {
	result, err = Processing(p, pencil)
	if err != nil {
		errors.Wrap(err, "failed starting pencil operation: procPencil")
		return nil, err
	}
	return result, err
}

func pencil(p IParams) (interface{}, error) {
	gocv.BitwiseNot(p.Mat, &p.Mat)
	return &p.Mat, nil
}
