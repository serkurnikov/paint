package imageProcessingService

import (
	"gocv.io/x/gocv"
	"image"
	"image/color"
)

const (
	//type space constants for calculation blend structure
	TypeLuv = iota
	TypeRGB
	TypeLinearRGB
	TypeLAB
	TypeLABCIE94
	TypeLABCIEDE2000
	TypeHSV
	TypeHCL

	DefaultCountOfColors = 5
	DefaultChannels      = 3
	DefaultDiff          = 0.2
	DefaultPieces        = 10
	DefaultIPointPool    = 500

	Line GeometricalShapeType = iota
	Rectangle
	Circle
)

//Proc Blending - calculate additive colors & mixing
//BlendStructure contains separation mColor information
//Blend base on idea: find channels colors & mix them (Accuracy average ~ 90-100%)
type (
	BlendStructure struct {
		C1Hex string
		C2Hex string
		C3Hex string

		C2Portion string
		C3Portion string

		ResultHex string
	}
)

//Proc Separation - different methods of calculation additive colors
//Separation base on idea: find similar color & constantly add necessary channels and shades(white, black)
//(Accuracy average ~ 15-30%)
type (
	SeparationStructure struct {
		AdditiveHex string

		ChHex      []string
		chPortions []float64

		ResultHex string
	}
)

//procColor - morph calculation, filters, mask, fetch colors areas
//ColorArea contains area information
//blending structure & image points (using in FetchColorArea, DisplayAreas)
type (
	ColorArea struct {
		Points   []*IPoint
		BlendStr BlendStructure
	}

	IPoint struct {
		R, C int
	}

	Palette struct{}

	SimilarClr struct {
		Diff float64
		Hex  string
	}

	//PyrMeanShiftFilter - create UnderPaint Image
	PyrShiftP struct {
		Sp, Sr   float32
		MaxLevel int32
	}
)

//Proc Geometrical - calculate geometrical shape & approximation
//GeometricalShapeType - image object type (Line, Rectangle, Circle...)
//GeometricalShape - struct of Shape
type (
	GeometricalShapeType int

	GeometricalShape struct {
		Type GeometricalShapeType
	}
)

//Proc obj
//ImageObject - struct describe single object in picture
type ImageObject struct {
	children []ImageObject
}

type ImageResult struct {
	index  int
	result *gocv.Mat
}

func (i ImageResult) Index() int {
	return i.index
}

func (i ImageResult) Result() *gocv.Mat {
	return i.result
}

type (
	BaseP struct {
		Mat    gocv.Mat
		Pieces int
	}

	//TODO replace IParams - not good idea to put such
	// - big struct as parameters
	IParams struct {
		BaseP
		FBGroundP
		LaplacianP
		MorphP
		PyrShiftP
		SobelP
		ThresholdP
		ContourP
		ContourCustom
		HoughLine
		HoughCircle
		Holes
	}
)

//Types for concurrent calculations
type (
	imageProcFn func(IParams) (interface{}, error)

	Calculation struct {
		prm IParams
		fn  imageProcFn
	}
)

//Proc morph - calculate morph func (erode, dilate, binary mask, threshold)
//support concurrency
type (
	FBGroundP struct {
		Shape                  gocv.MorphShape
		KernelSize, Iterations int
	}

	MorphP struct {
		Type                   gocv.MorphType
		Shape                  gocv.MorphShape
		Border                 gocv.BorderType
		KernelSize, Iterations int
	}

	ThresholdP struct {
		Thresh, Maxvalue float32
	}
)

//procContours - calculate contours func (HoughCircle & HoughLines)
//not support concurrency
type (
	CustomContour struct {
		c     [][]image.Point
		area  float64
		index int
	}
)

func (cc CustomContour) Len() int {
	return len(cc.c)
}

func (cc CustomContour) Less(i, j int) bool {
	aI := gocv.ContourArea(cc.c[i])
	aJ := gocv.ContourArea(cc.c[j])
	if aI > aJ {
		return true
	}
	return false
}

func (cc CustomContour) Swap(i, j int) {
	cc.c[i], cc.c[j] = cc.c[j], cc.c[i]
}

//ProcContours Structs
type (
	ContourP struct {
		Color                    color.RGBA
		T1, T2                   float32
		Thickness                int
		RetrievalMode            gocv.RetrievalMode
		ContourApproximationMode gocv.ContourApproximationMode
	}

	ContourCustom struct {
		Color                           color.RGBA
		T1, T2                          float32
		BlurSize, KernelSize, Thickness int
		SigmaX, SigmaY                  float64
		Border                          gocv.BorderType
		RetrievalMode                   gocv.RetrievalMode
		ContourApproximationMode        gocv.ContourApproximationMode
		Type                            gocv.MorphType
		Shape                           gocv.MorphShape
	}

	HoughLine struct {
		Color                                 color.RGBA
		Thickness                             int
		Rho, Theta, minLineLength, maxLineGap float32
		Threshold                             int
	}

	HoughCircle struct {
		Color                color.RGBA
		Thickness            int
		HMode                gocv.HoughMode
		Dp, MinDist, p1, p2  float64
		MinRadius, MaxRadius int
	}

	LaplacianP struct {
		Border       gocv.BorderType
		Delta, Scale float64
		Size         int
	}

	SobelP struct {
		Border       gocv.BorderType
		Delta, Scale float64
		KernelSize   int
	}

	Holes struct {
		Color                    color.RGBA
		Thickness                int
		RetrievalMode            gocv.RetrievalMode
		ContourApproximationMode gocv.ContourApproximationMode
	}
)
