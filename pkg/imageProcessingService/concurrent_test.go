package imageProcessingService

import (
	"gocv.io/x/gocv"
	"paint/pkg/utils"
	"testing"
)

func TestSplitMatV(t *testing.T) {
	oMat := gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadColor)
	if oMat.Empty() {
		t.Errorf("mat is empty: error splitMatV")
	}
	chunks := utils.SplitMatV(oMat, DefaultPieces)

	var images []utils.ImageResult
	for i := 0; i < len(chunks); i++ {
		images = append(images, ImageResult{i, &chunks[i]})
	}

	rMat, err := utils.MergeMatsV(images)
	if err != nil {
		t.Errorf("Cannot merge matsV:%v", err)
	}

	if oMat.Cols() != rMat.Cols() || oMat.Rows() != rMat.Rows() {
		t.Errorf("result mat uncorrect: original/result: cols: %v/%v, rows: %v/%v",
			oMat.Cols(), rMat.Cols(),
			oMat.Rows(), rMat.Rows())
	}
}

