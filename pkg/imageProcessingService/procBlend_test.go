package imageProcessingService

import (
	"encoding/json"
	"github.com/lucasb-eyer/go-colorful"
	"github.com/rohanthewiz/element"
	"paint/pkg/def"
	h "paint/pkg/htmlG"
	"paint/pkg/models"
	"sort"
	"strconv"
	"strings"
	"testing"
)

const (
	с0 = "#ff0000"
	с1 = "#00ff00"
	с2 = "#0000ff"
)

func TestColorSeparation(t *testing.T) {
	input, _, err := models.LoadData(DefaultChannels, "trainColorNN.csv")
	if err != nil {
		t.Errorf("Cannot load trainData %v", err)
	}

	var resultsColor []SeparationStructure
	var truePredicts float64

	for _, in := range input {
		mainColorS := colorful.Luv(in[0], in[1], in[2]).Hex()
		separation := ColorSeparation(mainColorS, MasterColors, TypeLuv)
		resultsColor = append(resultsColor, separation[0])
		if CheckClrDiffByHex(mainColorS, separation[0].ResultHex) < DefaultDiff {
			truePredicts++
		}
	}
	t.Logf("Color Separation Accurancy = %0.2f percent", truePredicts/float64(len(input)) * 100)
	GenSeparatedStrHtmlResult(input, resultsColor)
}

func TestBlendColors(t *testing.T) {
	result := BlendColors(с0, с1, DefaultNumberOfShades)

	var colors map[string]interface{}
	err := json.Unmarshal([]byte(result.String()), &colors)
	if err != nil {
		t.Errorf("Cannot unmarshal blendColors %v", err)
	}

	GenBlendClrHtmlResult(colors)
}

func TestBlend3Colors(t *testing.T) {
	result := Blend3Colors(Color3{C1: с0, C2: с1, C3: с2}, DefaultNumberOfShades)
	blendStructures := make([]BlendStructure, 0)

	for C2, child := range result.S(с0).ChildrenMap() {
		for C3, value := range child.ChildrenMap() {
			var blendColors map[string]interface{}
			_ = json.Unmarshal([]byte(value.String()), &blendColors)

			for shade, v := range blendColors {
				split := strings.Split(C2, def.Lattice)
				blendStructures = append(blendStructures, BlendStructure{
					C1Hex:     с0,
					C2Hex:     с1,
					C3Hex:     C3,
					C2Portion: split[0],
					C3Portion: shade,
					ResultHex: v.(string),
				})
			}
		}
	}
	GenBlend3ClrHtmlResult(blendStructures)
}

func TestStructureAmidFabricColors(t *testing.T) {

	input, _, err := models.LoadData(DefaultChannels, "trainColorNN.csv")
	if err != nil {
		t.Errorf("Cannot load trainData %v", err)
	}

	var resultsColor []BlendStructure
	var truePredicts float64

	for _, in := range input {
		mainColorS := colorful.Luv(in[0], in[1], in[2]).Hex()
		blend := StructAmidFabricColors(mainColorS, MasterColors, TypeLuv)
		resultsColor = append(resultsColor, blend[0])
		if CheckClrDiffByHex(mainColorS, blend[0].ResultHex) < DefaultDiff {
			truePredicts++
		}
	}
	t.Logf("BlendStructure Accurancy = %v percent", truePredicts/float64(len(input)) * 100)
	GenBlendStrHtmlResult(input, resultsColor)
}

func GenSeparatedStrHtmlResult(in [][]float64, structures []SeparationStructure) {
	var s string
	e := element.New

	for i, v := range structures {
		mClr := colorful.Luv(in[i][0], in[i][1], in[i][2]).Hex()
		s += e(
			h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(mClr)+h.WidthDef()).R(mClr)

		s += e(
			h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(v.ResultHex)+h.WidthDef()).R(v.ResultHex)

		s += e(
			h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(v.AdditiveHex)+h.WidthDef()).R(v.AdditiveHex)

		s += e(h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(v.ChHex[0])+h.Width(v.chPortions[0]*h.DefSizeBox)).R(v.ChHex[0])

		s += e(h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(v.ChHex[1])+h.Width(v.chPortions[1]*h.DefSizeBox)).R(v.ChHex[1])

		s += e(h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(v.ChHex[2])+h.Width(v.chPortions[2]*h.DefSizeBox)).R(v.ChHex[2])

		s += e(h.Br).R()
	}


	h.WritePage(h.Page{
		Name:  "Test Separated Color Result",
		Value: s,
	})
}

func GenBlendStrHtmlResult(in [][]float64, structures []BlendStructure) {
	e := element.New
	var s string

	for i, v := range structures {
		mClr := colorful.Luv(in[i][0], in[i][1], in[i][2]).Hex()
		s += e(h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(mClr)+h.WidthDef()).R(mClr)

		s += e(h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(v.ResultHex)+h.WidthDef()).R(v.ResultHex)

		s += e(h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(v.C1Hex)+h.WidthDef()).R(v.C1Hex)

		p2, _ := strconv.ParseFloat(v.C2Portion, 32)
		s += e(h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(v.C2Hex)+h.Width(p2*h.DefSizeBox)).R(v.C2Hex)

		p3, _ := strconv.ParseFloat(v.C3Portion, 32)
		s += e(h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(с2)+h.Width(p3*h.DefSizeBox)).R(с2)

		s += e(h.Br).R()
	}

	h.WritePage(h.Page{
		Name:  "Test Blend Structure",
		Value: s,
	})
}

func GenBlendClrHtmlResult(colors map[string]interface{}) {
	e := element.New
	var s string

	keys := make([]string, len(colors))
	i := 0
	for k := range colors {
		keys[i] = k
		i++
	}
	sort.Strings(keys)

	for _, k := range keys {
		s += e(h.Div, h.Class, h.BoxColor, h.Style,
			h.BG(с0)+h.WidthDef()).R(с0)

		s += e(h.Div, h.Class, h.BoxColor, h.Style,
			h.BG(с1)+h.WidthDef()).R(с1)

		s += e(h.Div, h.Class, h.BoxColor, h.Style,
			h.BG(colors[k])+h.WidthDef()).R(colors[k].(string))
		s += e(h.Br).R()
	}

	h.WritePage(h.Page{
		Name:  "Test Blend Color",
		Value: s,
	})
}

func GenBlend3ClrHtmlResult(structures []BlendStructure) {
	e := element.New
	var s string

	for _, v := range structures {
		s += e(h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(v.ResultHex)+h.WidthDef()).R(v.ResultHex)

		s += e(h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(v.C1Hex)+h.WidthDef()).R(v.C1Hex)

		p2, _ := strconv.ParseFloat(v.C2Portion, 32)
		s += e(h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(v.C2Hex)+h.Width(p2*h.DefSizeBox)).R(v.C2Hex)

		p3, _ := strconv.ParseFloat(v.C3Portion, 32)
		s += e(h.Div,
			h.Class, h.BoxColor,
			h.Style, h.BG(с2)+h.Width(p3*h.DefSizeBox)).R(с2)

		s += e(h.Br).R()
	}

	h.WritePage(h.Page{
		Name:  "Test Blend3Clr",
		Value: s,
	})
}

//BenchmarkStructAmidFabricColors
//BenchmarkStructAmidFabricColors-6   	       7	 153174686 ns/op	22996146 B/op	  568256 allocs/op
func BenchmarkStructAmidFabricColors(b *testing.B) {
	mainColorS := "#38312f"
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		StructAmidFabricColors(mainColorS, MasterColors, TypeLuv)
	}
	b.ReportAllocs()
}