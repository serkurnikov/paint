package imageProcessingService

import (
	"image"
)

type ObjectPaintInterface interface {
	FetchObjects(img image.Image) []ImageObject
	DisplayObject()
}
