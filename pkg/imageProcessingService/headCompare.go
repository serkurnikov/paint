package imageProcessingService

import "image"

type ComparePaintInterface interface {
	CompareDrawImage(img image.Image, imageState ImageState) float32
	DisplayErrors(img image.Image, imageState ImageState) *image.Image
}
