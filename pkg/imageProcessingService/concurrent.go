package imageProcessingService

import "C"
import (
	"gocv.io/x/gocv"
	"paint/pkg/utils"
	"reflect"
	"sort"
	"sync"
)

func SetupProc(vars map[interface{}]Calculation) error {
	ers := make(chan error, len(vars))
	var wg sync.WaitGroup
	for v, calculation := range vars {
		elem := reflect.ValueOf(v).Elem()
		if elem.Field(1).IsNil() {
			wg.Add(1)
			go func(prm IParams, setup imageProcFn) {
				mat, err := setup(prm)
				if err == nil {
					image := ImageResult{
						index:  int(elem.Field(0).Int()),
						result: mat.(*gocv.Mat),
					}
					elem.Set(reflect.ValueOf(image))
				}
				ers <- err
				wg.Done()
			}(calculation.prm, calculation.fn)
		}
	}
	wg.Wait()
	close(ers)
	for err := range ers {
		if err != nil {
			return err
		}
	}
	return nil
}

func Processing(p IParams, procFn imageProcFn) (result []byte, err error) {
	vars := initVars(p, procFn)
	err = SetupProc(vars)

	var images []utils.ImageResult
	for v := range vars {
		images = append(images, *v.(*ImageResult))
	}

	sort.Slice(images, func(i, j int) bool { return images[i].Index() < images[j].Index() })

	_, _ = utils.MergeMatsV(images)
	return nil, nil
}

func initVars(p IParams, procFn imageProcFn) (vars map[interface{}]Calculation) {
	chunks := utils.SplitMatV(p.Mat, p.Pieces)
	vars = make(map[interface{}]Calculation)

	for i := 0; i < len(chunks); i++ {
		imr := ImageResult{index: i}
		p.Mat = chunks[i]
		calculation := Calculation{prm: p, fn: procFn}

		vars[&imr] = calculation
	}
	return vars
}
