package imageProcessingService

import (
	"fmt"
	"github.com/lucasb-eyer/go-colorful"
	"gocv.io/x/gocv"
	"image"
	"image/color"
	"image/draw"
	"paint/pkg/utils"
	"testing"
)

func TestInitColors(t *testing.T) {
	instance := GetInstanceColors()
	mapOfAllColors := instance.GetMapOfAllColors()
	sortColorsByChls := instance.GetSortColorsByChls()
	if mapOfAllColors == nil || sortColorsByChls == nil {
		t.Errorf("instance is nil")
	} else {
		if len(mapOfAllColors[MasterColors]) != len(GetMastersColors()) {
			t.Errorf("uncorrect data in instance")
		}
	}
}

func TestDisplayInDominateClr(t *testing.T) {
	m := gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadColor)
	res := displayInDominatedClrs(&m, DefaultCountOfColors)
	gocv.IMWrite(utils.BuildImagePath("DominateClrs"), *res)
}

func TestDisplayColorArea(t *testing.T) {
	m := gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadColor)
	areas, _ := calculateClrAreas(&m, 11)

	dst := gocv.NewMatWithSizeFromScalar(
		gocv.NewScalar(255, 255, 255, 255),
		m.Rows(), m.Cols(),
		gocv.MatTypeCV8UC3)

	for i, area := range areas {
		res := displayColorArea(&dst, *area)
		gocv.IMWrite(utils.BuildImagePath(fmt.Sprintf("ClrArea_%v", i)), *res)
	}
}

func TestCalculateClrArea(t *testing.T) {
	m := gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadColor)
	_, _ = calculateClrAreas(&m, DefaultCountOfColors)
}

func TestAverageClr(t *testing.T) {
	img := image.NewRGBA(image.Rect(0, 0, HEIGHT, WEIGHT))
	clr, _ := colorful.Hex("#fffff")

	draw.Draw(img, img.Bounds(), &image.Uniform{C: clr}, image.Point{
		X: 0,
		Y: 0,
	}, draw.Src)

	p := make([]image.Point, 0)
	for x := 0; x < img.Bounds().Max.X; x++ {
		for y := 0; y < img.Bounds().Max.Y; y++ {
			p = append(p, image.Point{X: x, Y: y})
		}
	}

	resClr, err := averageClr(img, p)
	if err != nil {
		t.Errorf("cannot calculate average clr:%v", err)
	}

	diff := CheckClrDiffByHex(clr.Hex(), resClr.Hex())
	if diff != 0 {
		t.Errorf("uncorrect average clr, got%v, want %v. Diff = %v", resClr.Hex(), clr.Hex(), diff)
	}
}

func BenchmarkDisplayInDominateClr(b *testing.B) {
	m := gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadColor)
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		displayInDominatedClrs(&m, DefaultCountOfColors)
	}
}

func BenchmarkDisplayColorArea(b *testing.B) {
	m := gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadColor)
	areas, _ := calculateClrAreas(&m, DefaultCountOfColors)
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		displayColorArea(&m, *areas[0])
	}
}

func BenchmarkCalculateColorArea(b *testing.B) {
	m := gocv.IMRead(utils.BuildImagePath(TestImage), gocv.IMReadColor)
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = calculateClrAreas(&m, DefaultCountOfColors)
	}
}

func BenchmarkBuildPallet(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = buildPallet(DefaultCountOfColors)
	}
}

func BenchmarkGetSimilarClr(b *testing.B) {
	clrs := buildPallet(DefaultCountOfColors)
	c, _ := colorful.MakeColor(color.White)
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = getSimilarClr(&clrs, c)
	}
}
