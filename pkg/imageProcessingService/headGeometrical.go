package imageProcessingService

import "image"

type GeometricShapesInterface interface {
	DisplayGeometricShapes(img image.Image) []GeometricalShape
}