package imageProcessingService

import (
	"gocv.io/x/gocv"
	"image"
)

type ColorPaintInterface interface {
	CreateUnderPaintingImage(p IParams) *image.Image
	DisplayMainPalette(colors int) (*image.Image, Palette)
	DisplayColorsArea()
	SeparateColorIntoAdditive(mColorHex, colorFabric string) ([]BlendStructure, error)
}

func CreateUnderPaintImage(p IParams) ([]byte, error) {
	return PyrMeanShiftFilteringC(p)
}

func DisplayMainPalette(colors int) (*image.Image, Palette) {
	return nil, Palette{}
}

func DisplayColorArea(m *gocv.Mat, area ColorArea) *gocv.Mat {
	return displayColorArea(m, area)
}

func SeparateColorIntoAdditive(mColorHex, colorFabric string) ([]BlendStructure, error) {
	return StructAmidFabricColors(mColorHex, colorFabric, TypeLuv), nil
}
