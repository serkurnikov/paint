package imageProcessingService

import (
	"github.com/pkg/errors"
	"gocv.io/x/gocv"
	"image"
	"sort"
)

func LaplacianC(p IParams) (result []byte, err error) {
	result, err = Processing(p, laplacian)
	if err != nil {
		errors.Wrap(err, "failed starting laplacian operation: imageProcessingService")
		return nil, err
	}
	return result, err
}

func SobelC(p IParams) (result []byte, err error) {
	result, err = Processing(p, sobel)
	if err != nil {
		errors.Wrap(err, "failed starting sobel operation: imageProcessingService")
		return nil, err
	}
	return result, err
}

// Laplacian calculates the Laplacian of an image.
// borderType gocv.BorderType - default value is gocv.BorderDefault
// delta Optional delta value that is added to the results prior to storing them in dst {default = 0}
// For further details, please see:
// https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html#gad78703e4c8fe703d479c1860d76429e6
//
func laplacian(p IParams) (interface{}, error) {
	defer p.Mat.Close()

	dest := gocv.NewMat()

	gocv.Laplacian(p.Mat, &dest, gocv.MatTypeCV16S,
		p.LaplacianP.Size,
		p.LaplacianP.Scale,
		p.LaplacianP.Delta,
		p.LaplacianP.Border)

	return &dest, nil
}

// Sobel calculates the first, second, third, or mixed image derivatives using an extended Sobel operator
//
// For further details, please see:
// https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html#gacea54f142e81b6758cb6f375ce782c8d
//
func sobel(p IParams) (interface{}, error) {
	dest := gocv.NewMat()

	gocv.GaussianBlur(p.Mat, &p.Mat, image.Pt(
		p.SobelP.KernelSize,
		p.SobelP.KernelSize),
		1, 1,
		p.SobelP.Border)

	gocv.CvtColor(p.Mat, &p.Mat, gocv.ColorBGRAToGray)

	gradX := gocv.NewMat()
	absGradX := gocv.NewMat()
	defer gradX.Close()
	defer absGradX.Close()

	gradY := gocv.NewMat()
	absGradY := gocv.NewMat()
	defer gradY.Close()
	defer absGradY.Close()

	gocv.Sobel(p.Mat, &gradX, gocv.MatTypeCV16S, 1, 0,
		p.SobelP.KernelSize,
		p.SobelP.Scale,
		p.SobelP.Delta,
		p.SobelP.Border)

	gocv.Sobel(p.Mat, &gradY, gocv.MatTypeCV16S, 0, 1,
		p.SobelP.KernelSize,
		p.SobelP.Scale,
		p.SobelP.Delta,
		p.SobelP.Border)

	gocv.ConvertScaleAbs(gradX, &absGradX, 1, 0)
	gocv.ConvertScaleAbs(gradY, &absGradY, 1, 0)

	gocv.AddWeighted(absGradX, 0.5, absGradY, 0.5, 0, &dest)

	return &dest, nil
}

// FindContours finds contours in a binary image.
//
// For further details, please see:
// https://docs.opencv.org/3.3.0/d3/dc0/group__imgproc__shape.html#ga17ed9f5d79ae97bd4c7cf18403e1689a
// threshold1	first threshold for the hysteresis procedure.
// threshold2	second threshold for the hysteresis procedure.
func DrawDefContours(p IParams) (interface{}, error) {
	dest := gocv.NewMat()
	gocv.CvtColor(p.Mat, &dest, gocv.ColorBGRToGray)

	matCanny := gocv.NewMat()
	defer matCanny.Close()

	gocv.Canny(dest, &matCanny, p.ContourP.T1, p.ContourP.T2)
	contours := gocv.FindContours(matCanny, p.ContourP.RetrievalMode, p.ContourP.ContourApproximationMode)

	gocv.CvtColor(dest, &dest, gocv.ColorGrayToBGR)
	gocv.DrawContours(&dest, contours, -1, p.ContourP.Color, p.ContourP.Thickness)

	return &dest, nil
}

// HoughLinesP implements the probabilistic Hough transform
// algorithm for line detection. For a good explanation of Hough transform, see:
// http://homepages.inf.ed.ac.uk/rbf/HIPR2/hough.htm
//
// For further details, please see:
// http://docs.opencv.org/master/dd/d1a/group__imgproc__feature.html#ga8618180a5948286384e3b7ca02f6feeb
//
func DrawHoughLinesWithParams(p IParams) (interface{}, error) {
	dest := gocv.NewMat()
	defer dest.Close()

	gocv.CvtColor(p.Mat, &dest, gocv.ColorBGRToGray)

	matLines := gocv.NewMat()
	defer matLines.Close()

	gocv.HoughLinesPWithParams(
		dest,
		&matLines,
		p.HoughLine.Rho,
		p.HoughLine.Theta,
		p.HoughLine.Threshold,
		p.HoughLine.minLineLength,
		p.HoughLine.maxLineGap)

	for index := 0; index < matLines.Rows(); index++ {
		pt1 := image.Pt(int(matLines.GetVeciAt(index, 0)[0]),
			int(matLines.GetVeciAt(index, 0)[1]))

		pt2 := image.Pt(int(matLines.GetVeciAt(index, 0)[2]),
			int(matLines.GetVeciAt(index, 0)[3]))

		gocv.Line(&p.Mat, pt1, pt2, p.HoughLine.Color, p.HoughLine.Thickness)
	}

	return &p.Mat, nil
}

// HoughCirclesWithParams finds circles in a grayscale image using the Hough
// transform. The only "method" currently supported is HoughGradient.
//
// For further details, please see:
// https://docs.opencv.org/master/dd/d1a/group__imgproc__feature.html#ga47849c3be0d0406ad3ca45db65a25d2d
func DrawHoughCircles(p IParams) (interface{}, error) {
	dest := gocv.NewMat()
	defer dest.Close()

	gocv.CvtColor(p.Mat, &dest, gocv.ColorBGRToGray)

	matCircles := gocv.NewMat()
	defer matCircles.Close()

	gocv.HoughCirclesWithParams(dest, &matCircles,
		p.HoughCircle.HMode,
		p.HoughCircle.Dp,
		p.HoughCircle.MinDist,
		p.HoughCircle.p1,
		p.HoughCircle.p2,
		p.HoughCircle.MinRadius,
		p.HoughCircle.MaxRadius)

	for index := 0; index < matCircles.Cols(); index++ {

		point := image.Pt(
			int(matCircles.GetVecfAt(0, index)[0]),
			int(matCircles.GetVecfAt(0, index)[1]))

		radius := int(matCircles.GetVecfAt(0, index)[2])
		gocv.Circle(&p.Mat, point, radius, p.HoughCircle.Color, p.HoughCircle.Thickness)
	}

	return &p.Mat, nil
}

func DrawCustomContours(p IParams) (interface{}, [][]image.Point, error) {
	defer p.Mat.Close()

	blur := gocv.NewMat()
	defer blur.Close()

	gocv.GaussianBlur(p.Mat, &blur,
		image.Pt(p.ContourCustom.BlurSize, p.ContourCustom.BlurSize),
		p.ContourCustom.SigmaX,
		p.ContourCustom.SigmaY,
		p.ContourCustom.Border)

	//erode edges so that 'more is inside of the edge detection
	eroded := gocv.NewMat()
	defer eroded.Close()

	kernel := gocv.GetStructuringElement(
		p.ContourCustom.Shape,
		image.Pt(p.ContourCustom.KernelSize, p.ContourCustom.KernelSize))
	defer kernel.Close()

	gocv.Erode(blur, &eroded, kernel)

	//median blur to remove 'salt and pepper'
	medianBlur := gocv.NewMat()
	defer medianBlur.Close()

	gocv.MedianBlur(eroded, &medianBlur, 9)

	//use morphology to try and join lines up to create a continuous line
	morph := gocv.NewMat()
	defer morph.Close()

	gocv.MorphologyEx(medianBlur, &morph, p.ContourCustom.Type, kernel)

	edges := gocv.NewMat()
	defer edges.Close()

	//finally detect edges
	gocv.Canny(morph, &edges, p.ContourCustom.T1, p.ContourCustom.T2)
	//possibly instead of canny, use threshold?: threshold (gray, bw, 0, 255, THRESH_BINARY|THRESH_OTSU);

	contours := gocv.FindContours(
		edges,
		p.ContourCustom.RetrievalMode,
		p.ContourCustom.ContourApproximationMode)

	var toSort CustomContour
	toSort.c = contours
	//find the contour with the largest area
	sort.Sort(toSort)

	dest := gocv.NewMatWithSizeFromScalar(gocv.Scalar{
		Val1: 255,
		Val2: 255,
		Val3: 255,
	}, p.Mat.Rows(), p.Mat.Cols(), gocv.MatTypeCV8UC3)

	if len(toSort.c) > 0 {
		gocv.DrawContours(&dest,
			toSort.c, -1,
			p.ContourCustom.Color,
			p.ContourCustom.Thickness)
	}

	return &dest, toSort.c, nil
}

func GetHoles(p IParams) (interface{}, error) {
	dest := gocv.NewMat()
	gocv.CvtColor(p.Mat, &dest, gocv.ColorBGRToGray)

	binImage := gocv.NewMat()
	defer binImage.Close()

	gocv.Threshold(dest, &binImage, 128, 255, gocv.ThresholdBinaryInv)

	binImageInv := gocv.NewMat()
	defer binImageInv.Close()
	gocv.BitwiseNot(binImage, &binImageInv)

	contour := gocv.FindContours(
		binImageInv,
		p.Holes.RetrievalMode,
		p.Holes.ContourApproximationMode)

	gocv.DrawContours(&binImageInv, contour, 0,
		p.Holes.Color,
		p.Holes.Thickness)

	nt := gocv.NewMat()
	defer nt.Close()

	gocv.BitwiseNot(binImage, &nt)
	gocv.BitwiseOr(binImageInv, nt, &binImageInv)

	return &binImageInv, nil
}

func GrabCut(p IParams) {
	dest := gocv.NewMat()
	gocv.CvtColor(p.Mat, &dest, gocv.ColorBGRToGray)

	src := gocv.NewMat()
	defer src.Close()

	gocv.CvtColor(dest, &dest, gocv.ColorRGBAToBGR)
	dest.ConvertTo(&src, gocv.MatTypeCV8UC3)

	mask := gocv.NewMatWithSize(dest.Rows(), dest.Cols(), gocv.MatTypeCV8U)
	defer mask.Close()

	bgdModel := gocv.NewMat()
	defer bgdModel.Close()
	fgdModel := gocv.NewMat()
	defer fgdModel.Close()

	r := image.Rect(10, 10, 500, 500)

	gocv.GrabCut(src, &mask, r, &bgdModel, &fgdModel, 1, gocv.GCEval)
}
