package imageProcessingService

import "image"

type PencilPaintInterface interface {
	DisplayPencilImage(img image.Image) *image.Image
}

func DisplayPencilImage(p IParams) ([]byte, error) {
	return PencilC(p)
}
