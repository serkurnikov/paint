package imageProcessingService

const (
	Geometric ImageState = iota
	Pencil
	UnderPaint
)

type (
	ImageState int

	Api interface {
		GeometricShapesInterface
		PencilPaintInterface
		ColorPaintInterface
		ContoursApi
		ObjectPaintInterface
	}
)
