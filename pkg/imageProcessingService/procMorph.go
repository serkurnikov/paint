package imageProcessingService

import (
	"github.com/pkg/errors"
	"gocv.io/x/gocv"
	"image"
)

func MorphologyExC(p IParams) (result []byte, err error) {
	result, err = Processing(p, morph)
	if err != nil {
		errors.Wrap(err, "failed starting MorphologyExC operation: morphProcessing")
		return nil, err
	}
	return result, err
}

func ThresholdC(p IParams) (result []byte, err error) {
	result, err = Processing(p, threshold)
	if err != nil {
		errors.Wrap(err, "failed starting Threshold operation: morphProcessing")
		return nil, err
	}
	return result, err
}

func FBGroundC(p IParams) (result []byte, err error) {
	result, err = Processing(p, fBGround)
	if err != nil {
		errors.Wrap(err, "failed starting fbGround operation: morphProcessing")
		return nil, err
	}
	return result, err
}

// mShape - gocv.MorphShape {MorphRect, MorphCross, MorphEllipse}
// mType - gocv.MorphType {MorphErode, MorphDilate, MorphOpen, MorphClose, MorphGradient, MorphTophat, MorphBlackhat, MorphHitmiss}
// kernelSize - default value is 3
//
// For further details, please see:
// https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html#ga67493776e3ad1a3df63883829375201f
//
func morph(p IParams) (interface{}, error) {
	morphMat := gocv.NewMat()

	kernel := gocv.GetStructuringElement(p.MorphP.Shape, image.Pt(p.MorphP.KernelSize, p.MorphP.KernelSize))
	defer kernel.Close()

	gocv.MorphologyExWithParams(p.Mat, &morphMat, p.MorphP.Type, kernel, p.MorphP.Iterations, p.MorphP.Border)

	return &morphMat, nil
}

// Threshold applies a fixed-level threshold to each array element.
// thresh	threshold value. Example = 1
// maxvalue	maximum value to use with the THRESH_BINARY and THRESH_BINARY_INV thresholding types. Example = 128
//
// For further details, please see:
// https://docs.opencv.org/3.3.0/d7/d1b/group__imgproc__misc.html#gae8a4a146d1ca78c626a53577199e9c57
//
func threshold(p IParams) (interface{}, error) {
	defer p.Mat.Close()

	grayImage := gocv.NewMat()
	defer grayImage.Close()

	gocv.CvtColor(p.Mat, &grayImage, gocv.ColorBGRToGray)

	binImage := gocv.NewMat()

	gocv.Threshold(grayImage, &binImage, p.ThresholdP.Thresh, p.ThresholdP.Maxvalue, gocv.ThresholdBinary)

	return &binImage, nil
}

// FBGround search fore & back-ground using Watershed methods.
// nErode count of erode operation.
// nDilate count of dilate operation.
// mShape - gocv.MorphShape {MorphRect, MorphCross, MorphEllipse}
// mType - gocv.MorphType {MorphErode, MorphDilate, MorphOpen, MorphClose, MorphGradient, MorphTophat, MorphBlackhat, MorphHitmiss}
// kernelSize - default value is 3
//
// For further details, please see:
// https://docs.opencv.org/master/d7/d1b/group__imgproc__misc.html#ga3267243e4d3f95165d55a618c65ac6e1
//
func fBGround(p IParams) (interface{}, error) {
	defer p.Mat.Close()

	grayMat := gocv.NewMat()
	defer grayMat.Close()

	gocv.CvtColor(p.Mat, &grayMat, gocv.ColorBGRToGray)

	thresholdMat := gocv.NewMat()

	gocv.Threshold(grayMat, &thresholdMat, 0, 255, gocv.ThresholdOtsu)

	fg := gocv.NewMat()
	defer fg.Close()

	kernel := gocv.GetStructuringElement(p.FBGroundP.Shape, image.Pt(p.FBGroundP.KernelSize, p.FBGroundP.KernelSize))
	defer kernel.Close()

	gocv.ErodeWithParams(thresholdMat, &fg, kernel, image.Pt(-1, -1), p.FBGroundP.Iterations, 0)

	bgt := gocv.NewMat()
	defer bgt.Close()
	gocv.DilateWithParams(thresholdMat, &bgt, kernel, image.Pt(-1, -1), p.FBGroundP.Iterations, 0)

	bg := gocv.NewMat()
	defer bg.Close()
	gocv.Threshold(bgt, &bg, 1, 128, gocv.ThresholdBinaryInv)

	marker := gocv.NewMat()
	defer marker.Close()
	gocv.Add(fg, bg, &marker)

	markerCV32FC1 := gocv.NewMat()
	defer markerCV32FC1.Close()
	marker.ConvertTo(&markerCV32FC1, gocv.MatTypeCV32SC1)

	gocv.Resize(p.Mat, &p.Mat, image.Pt(markerCV32FC1.Cols(), markerCV32FC1.Rows()), 0, 0, gocv.InterpolationLinear)

	gocv.Watershed(p.Mat, &markerCV32FC1)

	m := gocv.NewMat()
	defer m.Close()
	gocv.ConvertScaleAbs(markerCV32FC1, &m, 1, 0)

	gocv.Threshold(m, &thresholdMat, 0, 255, gocv.ThresholdOtsu)

	dest := gocv.NewMat()
	defer dest.Close()

	p.Mat.CopyToWithMask(&dest, thresholdMat)

	return &thresholdMat, nil
}
