package imageProcessingService

import (
	"gocv.io/x/gocv"
	"paint/pkg/utils"
	"testing"
)

func TestDefContours(t *testing.T) {
	TestP.Mat = gocv.IMRead(utils.BuildImagePath("shiftFiltering"), gocv.IMReadColor)
	mat, err := DrawDefContours(TestP)
	result := *mat.(*gocv.Mat)
	defer result.Close()

	if err != nil {
		t.Errorf("Error DefContour %v", err)
	}
	gocv.IMWrite(utils.BuildImagePath("defContour"), result)
}

func TestCustomContours(t *testing.T) {
	TestP.Mat = gocv.IMRead(utils.BuildImagePath("shiftFiltering"), gocv.IMReadColor)
	mat, _, err := DrawCustomContours(TestP)
	result := *mat.(*gocv.Mat)
	defer result.Close()

	if err != nil {
		t.Errorf("Error CustomContours %v", err)
	}
	gocv.IMWrite(utils.BuildImagePath("customContours"), result)
}

func TestDrawHoughCircles(t *testing.T) {
	TestP.Mat = gocv.IMRead(utils.BuildImagePath("customContours"), gocv.IMReadColor)
	TestP.HoughCircle.MinDist = float64(TestP.Mat.Rows()/2)
	mat, err := DrawHoughCircles(TestP)
	result := *mat.(*gocv.Mat)
	defer result.Close()

	if err != nil {
		t.Errorf("Error HoughCircles %v", err)
	}
	gocv.IMWrite(utils.BuildImagePath("HoughCircles"), result)
}

func TestDrawHoughLinesWithParams(t *testing.T) {
	TestP.Mat = gocv.IMRead(utils.BuildImagePath("customContours"), gocv.IMReadColor)
	mat, err := DrawHoughLinesWithParams(TestP)
	result := *mat.(*gocv.Mat)
	defer result.Close()

	if err != nil {
		t.Errorf("Error HoughLines %v", err)
	}
	gocv.IMWrite(utils.BuildImagePath("HoughLines"), result)
}