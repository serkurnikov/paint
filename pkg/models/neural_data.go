package models

import (
	"encoding/json"
	"fmt"
	"paint/pkg/utils"
	"strconv"
)

//Model for fetching data from csv files (train & test.csv)
type InputData struct {
	LR float64 `csv:"lR"`
	UR float64 `csv:"uR"`
	VR float64 `csv:"vR"`
}

type OutData struct {
	LC1     float64 `csv:"lC1"`
	UC1     float64 `csv:"uC1"`
	VC1     float64 `csv:"vC1"`
	ShadeC2 float64 `csv:"ShadeC2"`
	LC2     float64 `csv:"lC2"`
	UC2     float64 `csv:"uC2"`
	VC2     float64 `csv:"vC2"`
	ShadeC3 float64 `csv:"ShadeC3"`
	LC3     float64 `csv:"lC3"`
	UC3     float64 `csv:"uC3"`
	VC3     float64 `csv:"vC3"`
}

type DataCSV struct {
	InputData  InputData
	TargetData OutData
}

func LoadData(inputPrams int, fPath string) (input, target [][]float64, err error) {
	csvLines, err := utils.ReadFileCSV("/go/src/gitlab.com/serkurnikov/paint/assets/neural/"+fPath)
	if err != nil {
		return nil, nil, err
	}

	for _, line := range csvLines {
		in := make([]float64, 0)
		trg := make([]float64, 0)

		for j := 0; j < len(line); j++ {
			value, _ := strconv.ParseFloat(line[j], 64)
			switch j < inputPrams {
			case true:
				in = append(in, value)
			case false:
				trg = append(trg, value)
			}
		}
		input = append(input, in)
		target = append(target, trg)
	}
	return input, target, err
}

func SaveData(dataCSVS []DataCSV, path string) error {
	res := make([][]string, 0)
	for _, data := range dataCSVS {
		input := utils.GetStructValues(data.InputData)
		target := utils.GetStructValues(data.TargetData)
		m := make([]string, 0)

		for _, v := range input {
			m = append(m,fmt.Sprint(v))
		}

		for _, v := range target {
			m = append(m,fmt.Sprint(v))
		}
		res = append(res, m)
	}
	return utils.WriteFileCSV(path, res)
}


func ExtractData(inputData []float64, targetData []float64) (in InputData, o OutData) {
	inBytes, _ := utils.FillStruct(&in, utils.ToSlice(inputData))
	oBytes, _ := utils.FillStruct(&o, utils.ToSlice(targetData))

	_ = json.Unmarshal(inBytes, &in)
	_ = json.Unmarshal(oBytes, &o)

	return in, o
}