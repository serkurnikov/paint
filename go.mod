module paint

go 1.15

require (
	github.com/Jeffail/gabs/v2 v2.6.0
	github.com/cenkalti/backoff/v4 v4.1.0
	github.com/disintegration/gift v1.2.1
	github.com/go-openapi/errors v0.20.0
	github.com/go-openapi/loads v0.20.2
	github.com/go-openapi/runtime v0.19.26
	github.com/go-openapi/spec v0.20.3
	github.com/go-openapi/strfmt v0.20.0
	github.com/go-openapi/swag v0.19.14
	github.com/go-openapi/validate v0.20.2
	github.com/golang/protobuf v1.5.1
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.2
	github.com/jessevdk/go-flags v1.4.0
	github.com/jmoiron/sqlx v1.3.1
	github.com/lib/pq v1.10.0
	github.com/lucasb-eyer/go-colorful v1.2.0
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/oliamb/cutter v0.2.2
	github.com/pkg/errors v0.9.1
	github.com/powerman/go-service-example v1.3.0
	github.com/powerman/goose/v2 v2.7.0
	github.com/powerman/must v0.1.0
	github.com/powerman/structlog v0.7.1
	github.com/pressly/goose v2.7.0+incompatible
	github.com/prometheus/client_golang v1.10.0
	github.com/rs/cors v1.7.0
	github.com/sebest/xff v0.0.0-20210106013422-671bd2870b3a
	github.com/streadway/amqp v1.0.0
	gocv.io/x/gocv v0.26.0
	golang.org/x/net v0.0.0-20210316092652-d523dce5a7f4
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.26.0
)
