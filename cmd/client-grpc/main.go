package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"log"
	api "paint/pkg/api/proto_files"
)

const (
	address = "localhost:10000"
)

func ExampleDrawDefaultContours(ctx context.Context, client api.ImageProcessingServiceClient) {
	req := api.ContoursRequest{}
	res, err := client.DrawDefaultContours(ctx, &req)
	if err != nil {
		log.Panicln(err)
	}
	fmt.Println(res.OutPicture)
}

func main() {
	ExampleDrawDefaultContours(context.Background(), NewImageProcessingClient())
}

func NewImageProcessingClient() api.ImageProcessingServiceClient {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	return api.NewImageProcessingServiceClient(conn)
}
